<?php 
$page = "about";
include '../include/header.php';
?>

<div Style="background-image: url(../images/about-bg.jpg);  background-size:cover;">
    <div class="container" style="background-color: white; color: black; padding: 2%; height:100%; width:65%;">
      <?php
      include '../include/aboutnav.php';
      ?>
           <h2 style="color:black;">Education</h2>
           <hr>
            <p><strong>Knowledge Sharing and Legislative Reform</strong></p>
                           
            <p>At its core QHVSG is committed to education and reform, which extends to:</p>
            <ul>
                <li>Student and Educational Groups</li>
                <li>Detectives</li>
                <li>Government and Community Agencies</li>
                <li>Healthcare Providers</li>
                <li>Community and Professional Sporting Clubs</li>
                <li>Other Anti-Violence Support Groups</li>
            </ul>
        
            <p>In particular, QHVSG is committed to working with the Queensland Police Service, educational groups and football and sporting clubs to ensure young people are educated about the long term and complex ramifications of violence. QHVSG utilises its signature awareness and education campaign One Punch Can Kill as a grassroots means of engaging with students and to date has presented to thousands of Queensland school students. Presentations are relevant and impactful, and schools, clubs and other organisations interested in hosting a One Punch Can Kill presentation should contact QHVSG to coordinate.</p>
        
            <p>Students interested in supporting QHVSG’s One Punch Can Kill campaign should visit – <a href="www.opck.qhvsg.org.au">www.opck.qhvsg.org.au</a>.</p>

     </div>
</div>
<?php 

include '../include/footer.php';
?>