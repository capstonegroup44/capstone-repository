

<?php
$page = "Loginchoice";

include '../include/connect.php';
include '../include/header.php';

?>

<div class="container text-center bg-grey " style="padding: 5%;">
	<div class="row box box-blue">
		<h1 style="color: grey">
			GET <strong>INVOLVED</strong>
		</h1>
		<hr>
		<div class="col-md-4 col-md-offset-2">

			<div class="thumbnail">
				<p class=" text-center">
					<span style="font-size: 1.8em;">BECOME A<br> <strong>VOLUNTEER</span></strong><br>
				</p>
				<img src="../images/4.jpg" alt="" width="150" height="150">

			</div>
			<a href="volregi.php" type="button" class="btn btn-default boxbtn1">
				<span class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>APPLY
				NOW
				</button>
			</a>
		</div>
		<div class="col-md-4 ">

			<div class="thumbnail">
				<p class=" text-center">
					<span style="font-size: 1.8em;">SIGN UP AS<br> <strong>MEMBER </span></strong><br>
				</p>
				<img src="../images/7.jpg" alt="" width="150" height="150">

			</div>
			<a href="registration.php" type="button"
				class="btn btn-default boxbtn1"> <span
				class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span>APPLY
				NOW
				</button>
			</a>
		</div>

	</div>
</div>

<?php
 include '../include/footer.php';
?>
