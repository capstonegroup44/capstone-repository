
<?php
$page = "update member";

include "../../include/connect.php";
include 'admin_header.php';


?>
<?php
$memberID = $_GET ['memberID'];
$sql = "SELECT * FROM member WHERE memberID = '$memberID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$row = mysqli_fetch_array ( $result );
$applifile = $row ['application'];
?>
<div class="content-wrapper">
	<h1>Update member</h1>
	<div class="row ">
		<div class="col-md-4 offset-md-8 ">
			<form action="memberupdateprocessing.php" method="post">
				<div class="form-group">
					<label>Username*</label> <input class="form-control" type="text"
						name="username" required value="<?php echo $row['username'] ?>"
						readonly />
				</div>

				<div class="form-group">
					<label>First Name*</label> <input class="form-control" type="text"
						name="firstname" required value="<?php echo $row['firstname'] ?>"
						readonly /><br />
				</div>

				<div class="form-group">
					<label>Last Name*</label> <input class="form-control" type="text"
						name="lastname" required value="<?php echo $row['lastname'] ?>"
						readonly /><br />
				</div>

				<div class="form-group">
					<label>contact number</label> <input class="form-control"
						type="text" name="contactnumber"
						value="<?php echo $row['mobile'] ?>" /><br />
				</div>

				<div class="form-group">
					<label>email</label> <input class="form-control" type="text"
						name="email" required value="<?php echo $row['email'] ?>" /><br />
				</div>


				<div class="form-group">
					<input type="hidden" name="memberID" class="form-control"
						value="<?php echo $memberID; ?>">
					<p>
						<input type="submit" name="memberupdate" value="Update member"
							class="form-control btn-submit" />
					</p>

				</div>
			</form>
			
					
		</div>
	</div>

</div>


