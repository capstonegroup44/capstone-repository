<?php 
include '../include/header.php';
?>
  <header id="home">
    <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">
        <div class="item active" style="background-image: url(../images/slider/1.jpeg)">
          <div class="caption">
            <h1 class="animated fadeInLeftBig"><span class="title_bg">QHVSG </span><span class="text-lowercase" color="#fff">is here for you...</h1>
            <p class="animated fadeInRightBig"></p>
            <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">See more..</a>
          </div>
        </div>
        <div class="item" style="background-image: url(../images/slider/2.jpg)">
          <div class="caption">
            <span style="font=size:30px;" class="animated fadeInLeftBig"> <h1 class="animated fadeInLeftBig"><span class="title_bg">Homocide</span></h1><h1 class="text-lowercase" color="#fff" style="font-size:40px">changes our world <br>suddenly and heartbreakingly..</h1>
            <p class="animated fadeInRightBig">
            <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">See more..</a>
          </div>
        </div>
      
      </div>
      <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
      <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>

      <a id="tohash" href="#services"><i class="fa fa-angle-down"></i></a>

    </div><!--/#home-slider-->
   
  </header><!--/#home-->
 
  <section id="services">
    <div class="container">
      <div class="heading wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="row">
          <div class="text-center col-sm-8 col-sm-offset-2">
            <h2>Our Services</h2>
            <p>Although nothing can take away your pain, it can help to know you are not alone. Having worked exclusively with victims of homicide for over a decade, QHVSG understands what you’re going through. Our staff and volunteers may be able to assist you in the following ways</p>
          </div>
        </div> 
      </div>
      <div class="text-center our-services">
        <div class="row">
          <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="service-icon">
              <i class="fa fa-comments-o"></i>
            </div>
            <div class="service-info">
              <h3>Referrals to legal advisors</h3>
              
            </div>
          </div>
          <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="450ms">
            <div class="service-icon">
              <i class="  fa fa-group"></i>
            </div>
            <div class="service-info">
              
              <h3>Court support</h3>
             
            </div>
          </div>
          <div class="col-sm-4 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="550ms">
            <div class="service-icon">
              <i class="fa fa-heart"></i>
            </div>
            <div class="service-info">
              <h3>24-hour phone support</h3>
             
            </div>
          </div>
          <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="650ms">
            <div class="service-icon">
              <i class="fa fa-phone"></i>
            </div>
            <div class="service-info">
              <h3>Emergency financial assistance</h3>
             
          </div></div>
          <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="750ms">
            <div class="service-icon">
              <i class="fa fa-calendar"></i>
            </div>
            <div class="service-info">
              <h3>Personal support / home visits</h3>
            
            </div>
          </div>
          <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="850ms">
            <div class="service-icon">
              <i class="fa fa-gift"></i>
            </div>
            <div class="service-info">
              <h3>Referrals to counseling services</h3>
              
            </div>
          </div>
        </div>
      </div>
    </div>




  </section><!--/#services-->
  <section id="about-us" class="parallax">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h2>About us</h2>
            <hr>
            <p>QHVSG offers 24 hour emotional support, personal advocacy and information to all people affected by homicides that occur in Queensland, Australia, regardless of age, gender, culture or religion.

</p>
            <p>QHVSG is one of Queensland’s most highly regarded community support services and is constantly evolving to meet the needs of its members and the wider community. To achieve this we host regular community awareness initiatives, legislative reviews, fundraising events, member activities, and media communications. We encourage you to subscribe to our e-newsletter or to bookmark this website, which will provide you with regular updates on what we are doing and how you can get involved.</p>
          </div>
        </div>
        <div class="col-sm-6" style="padding-top:50px;">
           <div class="about-info wow fadeInUp secondpara" data-wow-duration="1000ms" data-wow-delay="400ms">
      <p>“ As Patron of the Queensland Homicide Victims’ Support Group (QHVSG) I am honoured to welcome you to QHVSG’s online information portal. In a perfect world, there would be no need for QHVSG; however life is unpredictable and not always perfect. It is not until you are close to the families of a homicide victim that you have an appreciation of the lasting legacy caused by such a devastating act. I encourage you to explore the information contained within these pages and to reach out to QHVSG if you or someone you know has been affected by homicide ” </p> <p class="text-right"> Patron, Ian Stewart APM<br>
      – Commissioner of Police, Queensland Police Service</span></p>
      <p><a href="../pages/about.php">More about us...</a></p>
           </div>
          </div>
        </div>
      </div>
  </section><!--/#about-us-->

  <section id="portfolio">
    <div class="container">
      <div class="row">
        <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
          <h2>Our Gallery</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam</p>
        </div>
      </div> 
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3">
          <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="folio-image">
              <img class="img-responsive" src="../images/portfolio/1.jpg" alt="">
            </div>
            <div class="overlay">
              <div class="overlay-content">
                <div class="overlay-text">
                  <div class="folio-info">
                    <h3>Time Hours</h3>
                    <p>Design, Photography</p>
                  </div>
                  <div class="folio-overview">
                    <span class="folio-link"><a class="folio-read-more" href="#" data-single_url="portfolio-single.html" ><i class="fa fa-link"></i></a></span>
                    <span class="folio-expand"><a href="../images/portfolio/portfolio-details.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="400ms">
            <div class="folio-image">
              <img class="img-responsive" src="../images/portfolio/2.jpg" alt="">
            </div>
            <div class="overlay">
              <div class="overlay-content">
                <div class="overlay-text">
                  <div class="folio-info">
                    <h3>Time Hours</h3>
                    <p>Design, Photography</p>
                  </div>
                  <div class="folio-overview">
                    <span class="folio-link"><a class="folio-read-more" href="#" data-single_url="portfolio-single.html" ><i class="fa fa-link"></i></a></span>
                    <span class="folio-expand"><a href="../images/portfolio/portfolio-details.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="500ms">
            <div class="folio-image">
              <img class="img-responsive" src="../images/portfolio/3.jpg" alt="">
            </div>
            <div class="overlay">
              <div class="overlay-content">
                <div class="overlay-text">
                  <div class="folio-info">
                    <h3>Time Hours</h3>
                    <p>Design, Photography</p>
                  </div>
                  <div class="folio-overview">
                    <span class="folio-link"><a class="folio-read-more" href="#" data-single_url="portfolio-single.html" ><i class="fa fa-link"></i></a></span>
                    <span class="folio-expand"><a href="../images/portfolio/portfolio-details.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="600ms">
            <div class="folio-image">
              <img class="img-responsive" src="../images/portfolio/4.jpg" alt="">
            </div>
            <div class="overlay">
              <div class="overlay-content">
                <div class="overlay-text">
                  <div class="folio-info">
                    <h3>Time Hours</h3>
                    <p>Design, Photography</p>
                  </div>
                  <div class="folio-overview">
                    <span class="folio-link"><a class="folio-read-more" href="#" data-single_url="portfolio-single.html" ><i class="fa fa-link"></i></a></span>
                    <span class="folio-expand"><a href="../images/portfolio/portfolio-details.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="700ms">
            <div class="folio-image">
              <img class="img-responsive" src="../images/portfolio/5.jpg" alt="">
            </div>
            <div class="overlay">
              <div class="overlay-content">
                <div class="overlay-text">
                  <div class="folio-info">
                    <h3>Time Hours</h3>
                    <p>Design, Photography</p>
                  </div>
                  <div class="folio-overview">
                    <span class="folio-link"><a class="folio-read-more" href="#" data-single_url="portfolio-single.html" ><i class="fa fa-link"></i></a></span>
                    <span class="folio-expand"><a href="../images/portfolio/portfolio-details.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="800ms">
            <div class="folio-image">
              <img class="img-responsive" src="../images/portfolio/6.jpg" alt="">
            </div>
            <div class="overlay">
              <div class="overlay-content">
                <div class="overlay-text">
                  <div class="folio-info">
                    <h3>Time Hours</h3>
                    <p>Design, Photography</p>
                  </div>
                  <div class="folio-overview">
                    <span class="folio-link"><a class="folio-read-more" href="#" data-single_url="portfolio-single.html" ><i class="fa fa-link"></i></a></span>
                    <span class="folio-expand"><a href="../images/portfolio/portfolio-details.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="folio-item wow fadeInRightBig" data-wow-duration="1000ms" data-wow-delay="900ms">
            <div class="folio-image">
              <img class="img-responsive" src="../images/portfolio/7.jpg" alt="">
            </div>
            <div class="overlay">
              <div class="overlay-content">
                <div class="overlay-text">
                  <div class="folio-info">
                    <h3>Time Hours</h3>
                    <p>Design, Photography</p>
                  </div>
                  <div class="folio-overview">
                    <span class="folio-link"><a class="folio-read-more" href="#" data-single_url="portfolio-single.html" ><i class="fa fa-link"></i></a></span>
                    <span class="folio-expand"><a href="../images/portfolio/portfolio-details.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-3">
          <div class="folio-item wow fadeInLeftBig" data-wow-duration="1000ms" data-wow-delay="1000ms">
            <div class="folio-image">
              <img class="img-responsive" src="../images/portfolio/8.jpg" alt="">
            </div>
            <div class="overlay">
              <div class="overlay-content">
                <div class="overlay-text">
                  <div class="folio-info">
                    <h3>Time Hours</h3>
                    <p>Design, Photography</p>
                  </div>
                  <div class="folio-overview">
                    <span class="folio-link"><a class="folio-read-more" href="#" data-single_url="portfolio-single.html" ><i class="fa fa-link"></i></a></span>
                    <span class="folio-expand"><a href="../images/portfolio/portfolio-details.jpg" data-lightbox="portfolio"><i class="fa fa-search-plus"></i></a></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="portfolio-single-wrap">
      <div id="portfolio-single">
      </div>
    </div><!-- /#portfolio-single-wrap -->
  </section><!--/#portfolio-->

  <section id="team">
    <div class="container">
      <div class="row">
        <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
          <h2>Team</h2>
          <h4>
Board of Management </h4>
        </div>
      </div>
      <div class="team-members">
        <div class="row">
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="300ms">
              <div class="member-image">
                <img class="img-responsive" src="../images/team/1.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Tina Good</h3>
                <h4>President</h4>
                <p>"As Vice President I will continue to strengthen existing relationships, while forging new ones with community organisations and government departments in all states because I believe that a collaborative voice will yield greater results for our members, particularly those that find themselves battling interstate rules and regulations like my family." </p>
              </div>
              
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="500ms">
              <div class="member-image">
                <img class="img-responsive" src="../images/team/2.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Monique Ferrario</h3>
                <h4>Secretary</h4>
                <p>" As a Team Leader I am aware of the importance of peer support, and would like to continue engaging with members to involve them in the decisions that affect the organisation, and provide them with opportunities to help build a strong support network of peers."</p>
              </div>
              
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="800ms">
              <div class="member-image">
                <img class="img-responsive" src="../images/team/3.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Leanne Pullen</h3>
                <h4>General Member</h4>
                
              </div>
            
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="700ms">
              <div class="member-image">
                <img class="img-responsive" src="../images/team/4.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Sherrie Meyer</h3>
                <h4>General Member</h4>
                
              </div>
        
            </div>
          </div>
         
         </hr>

          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="800ms">
              <div class="member-image">
                <img class="img-responsive" src="../images/team/5.jpg" alt="">
              </div>
              <div class="member-info">
                <h3>Julie Waters</h3>
                <h4>General Member</h4>
                <p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt</p>
              </div>
        
            </div>
          </div>

           <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="800ms">
             
              <div class="member-info text-left">
                <h4>David Crew: Treasurer</h4>
               <h4> General Manager: Brett Thompson</h4>
<h4>First Class Accounting: Finance contractor</h4>
<h4>Deborah Taylor: Family Coordinator (South Eastern Region)</h4>
<h4>Berni Palings: Family Support Coordinator (Brisbane)</h4>
<h4>Leanne Murfitt: Family Support Coordinator (Central Region)</h4>
<h4>Ellie Kelly: Family Support Coordinator (Northern Region)</h4>
<h4>Elaine Henderson: Family Support Coordinator (Southern Region)</h4>
<h4>Leanne Murfitt: Volunteer & Court Support Coordinator (Queensland)</h4>
               
              </div>
        
            </div>
          </div>
        </div>
      </div>            
    </div>
  </section><!--/#team-->

 

  
  <section id="blog">
    <div class="container">
      <div class="row">
        <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
          <h2>What's New</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam</p>
        </div>
      </div>
      <div class="blog-posts">
        <div class="row">
          <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">
            <div class="post-thumb">
              <a href="#"><img class="img-responsive" src="images/blog/1.jpg" alt=""></a> 
            
              <div class="post-icon">
                <i class="fa fa-pencil"></i>
              </div>
            </div>
            <div class="entry-header">
              <h3><a href="#">ANNUAL GENERAL MEETING</a></h3>
              <span class="date">10 December 2016</span>
            
            </div>
            <div class="entry-content">
              <p>

                Congratulations to our new Board for 2016-17.
                The current members of the Board can be found on our Team Page. </p>
            </div>
          </div>
            <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">
            <div class="post-thumb">
              <a href="#"><img class="img-responsive" src="images/blog/1.jpg" alt=""></a> 
            
              <div class="post-icon">
                <i class="fa fa-pencil"></i>
              </div>
            </div>
            <div class="entry-header">
              <h3><a href="#">ANNUAL GENERAL MEETING</a></h3>
              <span class="date">10 December 2016</span>
            
            </div>
            <div class="entry-content">
              <p>

                Congratulations to our new Board for 2016-17.
                The current members of the Board can be found on our Team Page. </p>
            </div>
          </div>
            <div class="col-sm-4 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="400ms">
            <div class="post-thumb">
              <a href="#"><img class="img-responsive" src="images/blog/1.jpg" alt=""></a> 
            
              <div class="post-icon">
                <i class="fa fa-pencil"></i>
              </div>
            </div>
            <div class="entry-header">
              <h3><a href="#">ANNUAL GENERAL MEETING</a></h3>
              <span class="date">10 December 2016</span>
            
            </div>
            <div class="entry-content">
              <p>

                Congratulations to our new Board for 2016-17.
                The current members of the Board can be found on our Team Page. </p>
            </div>
          </div>
          </div>
         </div></div></section> 


          <section id="features" class="parallax">
    <div class="container">
      <div class="row count">
     
           <div class="row">
          <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h2 class="black">Special thanks to our Corporate Partners</h2>
             
          </div>
        </div>
         
      
         <div class="col-sm-3 col-xs-6 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
        
              <img class="img-responsive" src="../images/link/afl-masters.png" alt="">
         
        
         
        </div>
        <div class="col-sm-3 col-xs-6 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="500ms">
         <img class="img-responsive" src="../images/link/Crest-QLD.jpg" alt="">
                            
          
        </div> 
        <div class="col-sm-3 col-xs-6 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="700ms">
           <img class="img-responsive" src="../images/link/kedron-wavell.png" alt="">
       
        </div> 
        <div class="col-sm-3 col-xs-6 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="900ms">
          <img class="img-responsive" src="../images/link/McInnesWilson-Logo1.jpg" alt="">                  
      
        </div>                 
      </div>
    </div>
  </section><!--/#features-->

  <section id="contact">
 
    <div id="contact-us" class="parallax">
      <div class="container">
        <div class="row">
          <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
            <h2>Contact Us</h2>
             <p>24-HOUR FREECALL SUPPORT LINE: 1800 774 744</p>
          </div>
        </div>
        <div class="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div class="row">
            <div class="col-sm-6">
              <form id="main-contact-form" name="contact-form" method="post" action="#" >
                <div class="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="text" name="name" class="form-control" placeholder="Name" required="required" style="color:#000;">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="email" name="email" class="form-control" placeholder="Email Address" required="required" style="color:#000;" style="color:#000;">
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" name="subject" class="form-control" placeholder="Subject" required="required">
                </div>
                <div class="form-group">
                  <textarea name="message" id="message" class="form-control" rows="4" placeholder="Enter your message" required="required"></textarea>
                </div>                        
                <div class="form-group">
                  <button type="submit" class="btn-submit">Send Now</button>
                </div>
              </form>   
            </div>
            <div class="col-sm-6">
              <div class="contact-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
               
                <ul class="address">
                  <li><i class="fa fa-map-marker"></i> <span> Address:</span> PO Box 292
                  Lutwyche QLD 4030 </li>
                  <li><i class="fa fa-phone"></i> <span> Phone:</span> +61 (07) 3857 4744 </li>
                  <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:someone@yoursite.com"> qhvsg@qhvsg.org.au</a></li>
                  <li>
                  <div class="footer-social">
                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                            <a href="#" target="_blank"><i class="fa fa-youtube"></i></a>
                          
                        </div></li>
                </ul>
              </div>                            
            </div>
          </div>
        </div>
      </div>
    </div>        
  </section><!--/#contact-->

    <?php 
    include '../include/footer.php'?>