<?php 
$page = "about";
include '../include/header.php';
?>

<div Style="background-image: url(../images/about-bg.jpg);  background-size:cover;">
    <div class="container" style="background-color: white; color: black; padding: 2%; height:100%; width:65%;">
      <?php
      include '../include/aboutnav.php';
      ?>
           <h2 style="color:black;">Who are we?</h2>
           <hr>
            <p><strong>Who are our Members? What do we do?</strong></p>
<p>Formed over a decade ago, Queensland Homicide Victims’ Support Group’s (QHVSG) foundations have been built on passion, enthusiasm for change, commitment to cause, and the altruistic nature of its Management Committee, Staff and Volunteers. These characteristics have allowed QHVSG to develop a high quality and personalised support service to nearly 150 new families each year who experience first-hand the devastating effects of homicide.</p>

<p>Broadly, QHVSG delivers services to those who identify as having lost a loved one to homicide. The obvious examples of this include parents, grandparents, siblings, children and extended family of the deceased. However in reality, the term ‘loved one’ can extend beyond this immediate family. Often we find that close neighbours and friends, colleagues, and school and work mates suffer emotionally, physically and financially from the loss of someone to homicide. Such is the devastation and trauma of homicide that entire communities are often affected, and struggle to understand the horror of what has happened.</p>

<p>Whilst QHVSG focuses on promoting its services towards immediate family and friends, it is not uncommon to find staff and volunteers supporting several individuals – each with varying relationships to the deceased – as the result of each new homicide. QHVSG offers a truly unique service. For many living in the aftermath of homicide, healing is often accelerated from speaking with others who have ‘been there too’. Many victims of homicide believe the general community cannot understand what they are going through, and many more are unable to articulate the pain and anguish they are going through. With 70% of QHVSG’s volunteers and 40% of QHVSG’s current staff identifying as victims of homicide, QHVSG exemplifies its model of ‘peer support’. As one QHVSG member explained, joining QHVSG was like ‘stepping in out of the rain’.</p>

<p>QHVSG is not only an organisation with strong ties to the community and a commitment to promote the rights of homicide victims, but is fundamentally, a place of solace and refuge for those who are attempting to piece their lives back together after the tragic and senseless loss of someone they love.</p>

<p><strong>Who funds us?</strong></p>

<p>Whilst QHVSG receives partial funding from the Queensland Government, as a not for profit organisation we remain heavily reliant on the generosity of our members, corporate partners and private philanthropists. It is fair to say the broad impact homicide has on the Queensland community is well  recognised by government and community service groups, however QHVSG’s challenge is to raise awareness, and importantly, funding from the State’s corporate sector. In addition to private membership fees and government funding, QHVSG works tirelessly to raise funds through various public events and activities throughout the year – to show you support visit our GET INVOLVED page.</p>

<p><strong>Where does QHVSG get its referrals from?</strong></p>

<p>QHVSG’s main referrer is the Queensland Police Service. In 2002, QHVSG and the QPS implemented a “Victims of Homicide Notification Form” system. This means as soon as a homicide occurs, QHVSG are notified by the Queensland Police by way of fax/email. This system ensures homicide victims have access to much-needed support very soon after the homicide, and when it can be of most value. QHVSG often receives referrals from other sources including, word of mouth, the internet, the Department of Justice, the Department of Public Prosecutions, Legal Aid, Queensland Mental Health, the Royal Brisbane Hospital, or counselling services. If you, or a community or health service you are aware of, may be interested in referring individuals or families affected by homicide to QHVSG, please CONTACT US.</p>

<p><strong>What services does QHVSG offer?</strong></p>

<p>At QHVSG we offer a range of important services to support members of the community affected by homicide. Our services are highly personalised and have been identified as important to the recovery process for those impacted.</p>
    
    <ul>
    <li>Assistance with writing applications for compensation</li>
    <li>Emergency financial assistance</li>
    <li>24-hour phone support</li>
    <li>Court support</li>
    <li>Personal support / home visits</li>
    <li>Respite accommodation</li>
    <li>Support meetings</li>
    <li>Monthly newsletter</li>
    <li>Assistance with understanding the legal system</li>
    <li>Advice with housing issues</li>
    <li>Referrals to counseling services</li>
    <li>Referrals to other support services (i.e. drug or alcohol abuse)</li>
    <li>Respite accommodation for individuals and families</li>
    <li>Assistance with writing victim impact statements</li>
    <li>Personal advocacy for members</li>
    <li>Referrals to legal advisors</li>
    <li>Community education and awareness presentations</li>
    </ul>

<p><strong>Current Staffing</strong></p>

<p>QHVSG’s team has grown considerably since its early days and currently employs seven part-time and one full-time staff members, together with various “support roles” and including administration and executive staff. QHVSG’s team comprises of individuals with diverse qualifications and experiences, however we all share a common passion for our cause. When looking to recruit new team members we aim to attract mature minded persons who hold a strong degree of experience (either academic, life or work) in community / welfare matters, and have a strong sense of social justice. It is also highly advantageous to have experience within the fields of traumatic grief and loss. In addition to its core administrative and management teams, QHVSG relies heavily on a highly committed team of Volunteers to help achieve its organizational objectives. We currently boast a team of 50 trained Volunteers, with a further number supporting QHVSG on an irregular basis, for example to help with particular fundraising efforts.</p>

<p><strong>Legal System</strong></p>

<p>Members can now obtain a <a href="http://www.auscript.com/justice/courts-and-tribunals/queensland-courts/">court transcript</a> for free thanks to the presentence of QHVSG to change these laws. They need to approach QHVSG to assist them in making this application.</p>


     </div>
</div>
<?php 

include '../include/footer.php';
?>