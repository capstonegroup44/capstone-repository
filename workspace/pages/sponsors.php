<?php 
$page = "about";
include '../include/header.php';
?>

<div Style="background-image: url(../images/about-bg.jpg);  background-size:cover;">
    <div class="container" style="background-color: white; color: black; padding: 2%; height:100%; width:65%;">
      <?php
      include '../include/aboutnav.php';
      ?>
           <h2 style="color:black;">Our Sponsors</h2>
           <hr>
            <p><strong>Our Corporate Partners</strong></p>
            

        <p>QHVSG and the services it provides are heavily reliant on the generous support of its corporate and community partners. Whilst QHVSG receives funding from the Queensland Government there is a significant shortfall in what is required to maintain and grow our services in line with the increasing number of homicides occurring in Queensland each year.</p>

        <p>To our current partners we say THANK YOU for your courage and generosity in supporting a cause that affects so many Queenslanders and yet remains a mere whisper on the lips of many more. Organisations interested in becoming an official partner of QHVSG’s One Punch Can Kill campaign or who may wish to sponsor or hold an event in conjunction with our fundraising arm, Daisy Chain Foundation, should contact us directly.</p>

     </div>
</div>
<?php 

include '../include/footer.php';
?>