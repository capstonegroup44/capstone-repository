<?php 
$page = "about";
include '../include/header.php';
?>

<div Style="background-image: url(../images/about-bg.jpg);  background-size:cover;">
    <div class="container" style="background-color: white; color: black; padding: 2%; height:100%; width:65%;">
      <?php
      include '../include/aboutnav.php';
      ?>
           <h2 style="color:black;">Charter</h2>
           <hr>
            <p><strong>Our Purpose and Commitments</strong></p>
               

                <p>QHVSG is a Registered Not for Profit organization meaning we must comply with a strict code of ethics and governance at all times. It also means all donations of $2 or more to QHVSG are tax deductible.</p>
                
                <p><strong>Our mission</strong></p>
                
                <p>To provide confidential peer support, help, love and understanding to the surviving victims of homicide and to create awareness for the unique needs of these people victims, whilst also promoting education and reform.
                Click here to download our latest Annual Report</p>
                
                <p><strong>Our Vision</strong></p>
                
                <p>We are a haven of SOLACE offering meaningful peer support and a powerful advocate to survivors of homicide regardless of culture or location whilst promoting human caring and positive interaction in the community</p>
                
                <p><strong>Our Shared Values</strong></p>
                
                <ul>
                    <li>We recognise that the impact of homicide is devastating and far reaching.</li>
                    <li>We are committed to treating all people with compassion, dignity, respect and confidentially</li>
                    <li>We encourage inclusiveness and empower our members toward personal development through</li>
                </ul>
                
                <p><strong>Collaboration to Growth</strong></p>
                <ul>
                    <li>We address social justice issues vigorously with truth, integrity and fortitude</li>
                    <li>We demonstrate ethical behaviour with integrity through open communication, sensitivity and with a positive attitude</li>
                    <li>We adopt a holistic approach in our planning and operations</li>
                </ul>
                <p><strong>Our Objectives</strong></p>
                <ul>
                    <li>To employ effective and efficient systems to ensure the professional management of our human, physical and financial resources</li>
                    <li>To provide timely and personalised support to individuals and families enabling healing and growth</li>
                    <li>To educate and promote awareness of the impact and prevalence of homicide.</li>
                    <li>To vigorously advocate and seek reform on behalf of our members on individual, group and community issues</li>
                    <li>To establish partnerships and networks to facilitate change consistent with our members needs</li>
                    <li>To be proactive and innovative in all that we do</li>
                    <li>To provide opportunities for survivors to re-integrate into life after homicide through active involvement and volunteering</li>
                </ul>
                <hr>
                <h2 style="color:black;"> What you can expect from us</h2>
                <hr>
                <p><strong>Victims of Homicide</strong></p>
                <ul>
                    <li>To be treated with courtesy and respect at all times</li>
                    <li>Immediate and long-term access to support, within an empowering and healing framework</li>
                    <li>Appropriate and relevant referrals to other support agencies</li>
                    <li>To be heard, understood and taken seriously</li>
                    <li>Equity in accessibility to services throughout the state regardless of location</li>
                    <li>Equality in treatment regardless of age, gender, sexual orientation, disability or impairment, or lifestyle choices</li>
                    <li>Accurate information regarding your rights as a victim of homicide, including information on compensation</li>
                    <li>Assistance and at times, advocacy when liaising with government departments, private business and NGO’s</li>
                    <li>Your right to privacy will be upheld at all times, including safe and secure storage of your personal records (both electronic and hard copy)</li>
                    <li>Details of your membership and/or involvement with QHVSG to remain confidential with regards to other member enquiries, stakeholder enquiries, enquiries from the media and any other third party.</li>
                </ul>
                <p><strong>Other Victims of Crime</strong></p>
                <ul>
                    <li>To be heard and responded to in a respectful and non-judgemental manner </li>
                    <li>Provision of appropriate and relevant information concerning your grievance </li>
                    <li>Appropriate and relevant referral to other agencies, including support, law and justice, and counselling </li>
                </ul>
                <p><strong>Associated Members</strong></p>
                <ul>
                    <li>Inclusion in QHVSG’s Annual General Meeting</li>
                    <li>Access to QHVSG’s yearly annual reports</li>
                    <li>Transparency with QHVSG’s operations (with the exception of not breaching members’ privacy)</li>
                    <li>To have your ideas and feedback heard, understood, and considered by the Committee of Management</li>
                    <li>Your right to privacy will be upheld at all times</li>
                    <li>Your personal records (both electronic and hard copy) shall be stored in a safe and secure location</li>
                </ul>
                <p><strong>Members of Queensland Police and the Department of Justice</strong></p>
                <ul>
                    <li>A collaborative approach to working with victims of homicide</li>
                    <li>A non-invasive manner regarding the investigation of families’ cases</li>
                    <li>Long-term, cooperative approach to working with members of the Queensland Police Service</li>
                </ul>
                <p><strong>Students</strong></p>
                <ul>
                    <li>Timely follow up on requests for information</li>
                    <li>Accurate and relevant information provided through QHVSG’s website</li>
                    <li>Opportunities to participate in QHVSG activities (when this does not interfere with QHVSG’s workload or vision)</li>
                </ul>
                <p><strong>Volunteers</strong></p>
                <ul>
                    <li>Clear communication on the types of roles available, and what these roles require (e.g. skills, experience, level of commitment)</li>
                    <li>Thorough induction process, and subsequent opportunities for professional development</li>
                    <li>To have access to varied, fulfilling and interesting work.</li>
                    <li>To a physically and emotional safe and healthy work environment.</li>
                    <li>To either accept or refuse jobs.</li>
                    <li>To be heard, to share ideas and contribute towards the growth of the organisation.</li>
                    <li>To learn, grow and develop skills needed to perform the various organizational jobs.</li>
                    <li>To be reimbursed for agreed out of pocket expenses.</li>
                    <li>Adequate resources for the work assignments.</li>
                    <li>Respect, supervision and support from the organisation and coworkers.</li>
                    <li>Recognition and encouragement.</li>
                    <li>Your right to privacy will be upheld at all times.</li>
                    <li>Details of your membership and/or involvement with QHVSG to remain confidential with regards to other member enquiries, stakeholder enquiries, enquiries from the media and any other third party.</li>
                    <li>Your personal records (both electronic and hard copy) shall be stored in a safe and secure location</li>
                </ul>
                <p><strong>Members of the Community</strong></p>
                <ul>
                    <li>Access to information concerning the plight of homicide victims</li>
                    <li>Opportunities to engage with QHVSG in a peaceful yet educational manner</li>
                    <li>Victims to be represented in an empowering manner within the community</li>
                    <li>Inter-agency collaboration when considering community violence and crime prevention and awareness strategies</li>
                </ul>
                <p><strong>Continuous Improvement</strong></p>
                <ul>
                    <li>Ongoing evaluation of QHVSG’s services, mission, vision, goals and objectives</li>
                    <li>Responsiveness to feedback from QHVSG’s members, stakeholders and members of the community</li>
                    <li>Benchmarking against state, national, and international victim support service providers</li>
                    <li>Yearly strategic planning meeting to review and evaluate effectiveness of service when meeting the changing demands of victims of homicide</li>

                <p><strong>How are we accountable?</strong></p>
                <ul>
                    <li>Publish our results in an Annual Report</li>
                    <li>Transparency in our complaints process</li>
                    <li>Yearly performance appraisal</li>
                    <li>Monitoring our performance against this charter</li>
                </ul>
                <p><strong>What you can do for us</strong></p>
                <ul>
                    <li>Tell us your needs</li>
                    <li>Tell us if you have a complaint, we will respond promptly and seriously to problems.</li>
                    <li>We would like you feedback, both positive and negative. Staff will ensure that your views and suggestions will be considered.</li>
                </ul>


     </div>
</div>
<?php 

include '../include/footer.php';
?>