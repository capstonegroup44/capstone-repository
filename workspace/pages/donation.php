<?php
include '../include/connect.php';
include '../include/header.php';

?>


<div class="container">
	<div class=" box box-blue">

		<h2>Donation</h2>
		<hr>
   
    <p class="big_letter center-block" style="width:500px; padding:15px;text-align:justify;">


    Donations are vital to QHVSG’s ongoing operations and increasing members support services. All donations, regardless of amount, are graciously appreciated, and every dollar donated to QHVSG goes towards ensuring individuals and families devastated by the tragedy of homicide, receive the very best level of specialist care and support. Please remember QHVSG is a ‘DGR’ registered not-for-profit charity and all donations of $2 or more are tax deductible.
    </p>
    <div class="row donate">
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">Online Donation</div>
                      <div class="panel-body">
                        <div class="row">

       <div class=" col-md-4 col-sm-4">
         <div class="thumbnail">
           <img src="http://placehold.it/500x300" alt="">
             <div class="caption">
               <h4>PRODUCT1</h4>
               Lorem ipsum dolor sit amet
              <button type="button" class="btn btn-hot text-uppercase btn-xs">CART <span class="glyphicon glyphicon-heart" aria-hidden="true"></span></button>
           </div>
         </div>
       </div>
       <div class=" col-md-4 col-sm-4">
         <div class="thumbnail">
           <img src="http://placehold.it/500x300" alt="">
             <div class="caption">
               <h4>PRODUCT2</h4>
               Lorem ipsum dolor sit amet
              <button type="button" class="btn btn-hot text-uppercase btn-xs">CART <span class="glyphicon glyphicon-heart" aria-hidden="true"></span></button>
           </div>
         </div>
       </div>
                      </div>
                </div>
            </div></div>
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading ">Electronic Funds Transfer</div>
                    <div class="panel-body text-left">
                      <dl>
                        <dt>
                      BOQ</dt>
<dd>BSB:124-009</dd>
<dd>Account:20132624</dd>
<dd>Reference: Donation – FIRST NAME SURNAME</dd>
                    </div>
                </div>


                <div class="panel panel-success">
                    <div class="panel-heading ">Cheque</div>
                    <div class="panel-body text-left">
                        Please make cheques payable to ‘QHVSG’ and return to:
                      <dt>Finance Officer</dt>


<dt>QHVSG</dt>
<dt>PO Box 292</dt>
<dt>Lutwyche QLD 4030</dt>
                    </div>
                </div>

        </div></div>

  </div>
</div>
<?php

include '../include/footer.php';
?>

