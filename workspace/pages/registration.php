
<?php
$page = "Registration";
include '../include/connect.php';
include '../include/header.php'; //

?>


	<?php
	
	if (isset ( $_SESSION ['member'] )) {
		echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('You already Loggined')
        window.location.href='memberlanding.php'
        </SCRIPT>");
	} else {
		
		?>
<div class="container">
	<div class="row box">
		
			<hr>
			<h2>Become a member</h2>
			<hr>

<?php
		// user messages
		if (isset ( $_SESSION ['error'] )) {
			echo '<div class="error">';
			echo '<p>' . $_SESSION ['error'] . '</p>';
			echo '</div>';
			unset ( $_SESSION ['error'] );
		}
		?><div class="col-md-6 ">
				<form action="registrationprocessing.php" method="post"
					enctype="multipart/form-data">
					
					<p>Membership to QHVSG is by application only and subject to review
						and approval by QHVSG Board of Management. To submit your
						application please complete all details below and submit this form
						together with your payment. Any application deemed unacceptable
						will have full monies refunded immediately.</p>
					<p>Passwords must have a minimum of 8 characters.</p>
					<label>Username*</label> <input type="text" name="username"
						class="form-control" required /></br> <label>Email*</label> <input
						type="email" class="form-control" name="email" required /><br /> <label>Password*</label>
					<input type="password" name="password" class="form-control"
						required pattern=".{8,}"
						title="Password must be 8 characters or more" /><br /> <label>upload
						application form</label><input class="form-control" type="file"
						name="file" /> <br>
					<p></p>
					</br>
					</br>
					<p>
						<input type="submit" class="btn btn-primary" name="registration"
							class="form-control" value="Create New Account" />


					</p>
					<p>*If you send a member application form by mail then we will send
						a temporary passward to your e-mail address. If you don't receive
						the any message then contact us.</p>
				</form>
			
		</div>
		<div class=" col-md-3 block-center">
			<a href="../file/2017-Membership-Application.pdf" download><button
					class="btn btn-warning downappl">
				Download Membership application form
				</button> </a></div>
	</div>
</div>



<?php }?>
<?php
include '../include/footer.php';
?>