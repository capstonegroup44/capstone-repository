<!-------------------------------------------------------

Subject: cab398 Group: Group 44
Webpage: registraionprocessing.php
File Version: 1.0.1 
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:registration processing
--------------------------------------------------------->
<?php
session_start ();
include "../include/connect.php";
?>
<?php
$username = mysqli_real_escape_string ( $con, $_POST ['username'] ); // prevent SQL injection
$password = mysqli_real_escape_string ( $con, $_POST ['password'] );
$email = mysqli_real_escape_string ( $con, $_POST ['email'] );

if ($_FILES ['file'] ['name']) // if an image has been uploaded
{
	
	$file = $_FILES ['file'] ['name']; // the PHP file upload variable for a file
	$randomDigit = rand ( 0000, 9999 ); // generate a random numerical digit <= 4 characters
	$newfileName = strtolower ( $randomDigit . "_" . $file ); // attach the random digit to the front of uploaded images to prevent overriding files with the same name in the images folder and enhance security
	$target = "../file/" . $newfileName; // the target for uploaded images
	$allowedExts = array (
			'pdf',
			'docx',
			'doc',
			'jpg' 
	); // create an array with the allowed file extensions
	$tmp = explode ( '.', $_FILES ['file'] ['name'] ); // split the file name from the file extension
	$extension = end ( $tmp );
	
	if ($_FILES ['file'] ['size'] > 512000) // image maximum size is 500kb
{
		echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('filesize ')
         window.location.href='registration.php'
        </SCRIPT>");
	} else {
		move_uploaded_file ( $_FILES ['file'] ['tmp_name'], $target ); // move the image to images folder
	}
}

if (strlen ( $password ) < 8) // check if the password is a minimum of 8 characters long
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Password must be more than 8 charaters or number ')
         window.location.href='registration.php'
        </SCRIPT>");
}

$salt = md5 ( uniqid ( rand (), true ) ); // create a random salt value
$password = hash ( 'sha256', $password . $salt ); // generate the hashed password with the salt value

$sql = "(SELECT username FROM member WHERE member.username='$username') UNION
(SELECT username FROM admin WHERE admin.username='$username')"; // check if the username is taken in the member table or the admin table as the username must be unique
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$numrow = mysqli_num_rows ( $result ); // count how many rows are returned

if ($numrow > 0) // if count greater than 0
{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Username taken. ')
         window.location.href='registration.php'
        </SCRIPT>");
} elseif ($username == "" || $password == "" || $email == "") // check if all required fields have data

{
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Please enter a valid email address')
         window.location.href='registration.php'</SCRIPT>");
} elseif (! filter_var ( $email, FILTER_VALIDATE_EMAIL )) // check if email is valid
{
	// if an error occurs intialise a session called 'error' with a
	
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Please enter a valid email address')
         window.location.href='registration.php'
        </SCRIPT>");
} else {
	$sql = "INSERT INTO member (username, password, salt, email,  date,
 type,application,applyMethod) VALUES ('$username', '$password', '$salt', 
  '$email',  NOW(), '1' ,'$newfileName','1')";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('successfully sign up')
 
        window.location.href='login.php'
        </SCRIPT>");
}
?>
