<!DOCTYPE html>
<?php
session_start ();
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>QHVSG</title>
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link href="../css/animate.min.css" rel="stylesheet"> 
  <link href="../css/font-awesome.min.css" rel="stylesheet">
  <link href="../css/lightbox.css" rel="stylesheet">
  <link href="../css/main.css" rel="stylesheet">
  <link id="css-preset" href="../css/presets/preset1.css" rel="stylesheet">
  <link href="../css/responsive.css" rel="stylesheet">
   <link href="../css/style_nav.css" rel="stylesheet">

  <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
 
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="../images/favicon.ico">
  <script type='text/javascript' src='http://code.jquery.com/jquery-1.7.1.js'></script>
</head><!--/head-->

<body>
<script type="text/javascript">
var lastScrollY = 0;
$(function(){
 var diffY = Math.max(document.documentElement.scrollTop, document.body.scrollTop);


 $(window).scroll(function(){
  var diffTop = Math.max(document.documentElement.scrollTop, document.body.scrollTop);
  if (diffY != lastScrollY) {
   percent = .10 * (diffY - lastScrollY);
   if (percent > 0) {
    percent = Math.ceil(percent);
   } else {
    percent = Math.floor(percent);
   }
   
   diffTop = parseInt($("#quick").offset().top) + percent;
   lastScrollY = lastScrollY + percent;
  }
  $("#quick").stop();
  $("#quick").animate({"top": diffTop}, 500);
 });
});

</script><a href="../pages/donation.php">
<div id="quick"  style="position:absolute; top:0; margin-top:740px; right:0;margin-right:10px; margin-left:500px ; width:150px; height:100px; text-align:center; z-index:1">

          <h3 style="color: #fff;"><span class="glyphicon glyphicon-heart"></span> MAKE A DONATION</h3>
      
</div>  </a>
<div class="header-area">
        <div class="container">
            <div class="row">
       
                
<ul class="list-inline " style="padding-left: 20px">
                         
                         <li class="list-inline-item pull-left text-info">24-HOUR FREECALL SUPPORT LINE: 1800 774 744</li>
                           
              <?php
                    if ((isset ( $_SESSION ['member'] ))) 
                    // check to see if a member or admin is logged in and, if so, display the logged in menu items
                    {
                      ?>
                     
                  <li class="list-inline-item pull-right"> <a href="memberlanding.php" style="" >My Account</a></li>
                  <li class="list-inline-item pull-right"><a href="logout.php">Logout</a></li>
             <?php
                    }
                    else{ 
                      echo ' <li class="list-inline-item pull-right" >  <a href="../pages/login.php" > LOGIN </a></li>';
                    }

                     ?>

              
                
                     
     
    <!-- end navigation -->

                          <li class="list-inline-item pull-right">  <a href='admin/admin_login.php' class=''> ADMIN </a></li>
                        </ul>
                    </div>
               </div>
     <!-- End header area -->


   
 <div class="main-nav" style="margin:0px;">
      <div class="container" >
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="logo" href="index.php" >
            <h1><span style="color:#fff; font-weight:900"; >QHVSG</h1>
          </a>                    
        </div>
        <div class="container">
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right" style="padding-top:10px">                 
            <li class=""><a href=about.php>About Us</a></li>
            <li class=""><a href="#">How to help</a></li> 
            <li class=""><a href="#">Shop</a></li>                     
            <li class=""><a href="#">Gallery</a></li>
            <li class=""><a href="#">Chat</a></li>
            <li class=""><a href="#">Blog</a></li>
            <li class=""><a href="contact.php">Contact</a></li>       
          </ul>
        </div>
      </div>
      </div>

    </div><!--/#main-nav-->